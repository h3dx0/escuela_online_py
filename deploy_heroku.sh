#!/bin/bash
git add .

git commit -m "commit to deploy"
echo "-------------------------------"
echo "---                         ---"
echo "----commit MASTER terminado---"
echo "---                         ---"
echo "---                         ---"

git push origin master
echo "-------------------------------"
echo "---                         ---"
echo "----push MASTER terminado---"
echo "---                         ---"
echo "-------------------------------"

git push heroku master
echo "-------------------------------"
echo "---                         ---"
echo "----push heroku terminado---"
echo "---                         ---"
echo "-------------------------------"
heroku run python3.6 manage.py migrate
echo "-------------------------------"
echo "---                         ---"
echo "----heroku migrated         ---"
echo "---                         ---"
echo "-------------------------------"
