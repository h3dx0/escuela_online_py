"""escuela_online_web URL Configuration
"""
from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('escuela_online.frontend.urls', namespace="frontend")),
    path('accounts/', include('django.contrib.auth.urls')),
]
