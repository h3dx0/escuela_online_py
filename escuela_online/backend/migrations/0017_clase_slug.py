# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-10 15:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0016_categoria_especialidad'),
    ]

    operations = [
        migrations.AddField(
            model_name='clase',
            name='slug',
            field=models.SlugField(blank=True, editable=False, max_length=200, null=True),
        ),
    ]
