from django.apps import AppConfig


class BackendConfig(AppConfig):
    name = 'escuela_online.backend'
