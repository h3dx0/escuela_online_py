from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import Transpose
from escuela_online.utils import PathAndRename
from django.contrib.auth.models import AbstractUser, UserManager
from django.template.defaultfilters import slugify

import binascii
import os

path_and_rename_imagen_curso = PathAndRename("cursos")
path_and_rename_imagen_usuario = PathAndRename("usuario")
app_label = 'backend'
# Create your models here.

ESTADOS_CURSO = (
    (0, 'abierto'),
    (1, 'pausa'),
    (2, 'cerrado'),
)

MONEDAS = (
    (0, 'MXN'),
    (1, 'USD'),
    (2, 'EUR'),
)

TIPO_DURACION = (
    (0, 'HORAS'),
    (1, 'DIAS'),
    (2, 'SEMANAS'),
    (3, 'MESES'),
)

TIPO_RECURSO = (
    (0, 'PDF'),
    (1, 'PPT'),
    (2, 'DOC'),
    (3, 'VIDEO'),
)


class Usuario(AbstractUser):
    objects = UserManager()
    telefono = models.IntegerField(blank=True, null=True)
    avatar = ProcessedImageField(max_length=255, blank=True, null=True,
                             upload_to=path_and_rename_imagen_usuario,
                             processors=[Transpose()])
    class Meta:
        app_label = 'backend'


class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    user = models.OneToOneField(Usuario, related_name='auth_token', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'backend'

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    @staticmethod
    def generate_key():
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key


class Curso(models.Model):
    titulo = models.CharField(max_length=300)
    categoria = models.ForeignKey('Categoria', related_name='cursos', on_delete=models.PROTECT)
    estado = models.IntegerField(default=0, choices=ESTADOS_CURSO)
    imagen_principal = ProcessedImageField(max_length=255, blank=True, null=True,
                                 upload_to=path_and_rename_imagen_curso,
                                 processors=[Transpose()])
    descripcion = models.TextField()
    video = models.CharField(max_length=255, blank=True, null=True)
    precio = models.IntegerField(default=0)
    moneda = models.IntegerField(default=0, choices=MONEDAS)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_inicio = models.DateTimeField(null=True, blank=True)
    tiempo_duracion = models.ForeignKey('TiempoDuracion', null=True, blank=True, on_delete=models.PROTECT)
    profesor = models.ManyToManyField('Profesor', related_name='cursos')
    estudiantes = models.ManyToManyField('Estudiante', related_name='cursos', blank=True)
    premium = models.BooleanField(blank=True, default=0)
    slug = models.SlugField(max_length=200, editable=False, unique=True)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return "{}-{}".format(self.titulo, self.categoria)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        return super(Curso, self).save(*args, **kwargs)


class Modulo(models.Model):
    nombre = models.CharField(max_length=300)
    curso = models.ForeignKey('Curso', related_name='modulos', on_delete=models.PROTECT)
    descripcion = models.TextField(null=True, blank=True)
    video = models.CharField(max_length=255, blank=True, null=True)
    tiempo_duracion = models.ForeignKey('TiempoDuracion', null=True, blank=True, on_delete=models.PROTECT)
    slug = models.SlugField(max_length=200, editable=False, null=True, blank=True)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return "{}-{}".format(self.nombre, self.curso)

    def get_duracion(self):
        return self.tiempo_duracion

    def get_cantidad_clases(self):
        return self.clases.count()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Modulo, self).save(*args, **kwargs)


class Clase(models.Model):
    nombre = models.CharField(max_length=300)
    modulo = models.ForeignKey('Modulo', related_name='clases', null=True, blank=True, on_delete=models.PROTECT)
    descripcion = models.TextField()
    tiempo_duracion = models.FloatField(default=0.0, help_text='en horas')
    es_gratis = models.BooleanField(default=False)
    slug = models.SlugField(max_length=200, editable=False, null=True, blank=True)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return "{}-{}".format(self.modulo, self.nombre)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Clase, self).save(*args, **kwargs)

class Recursos(models.Model):
    titulo = models.CharField(max_length=300)
    enlace = models.CharField(max_length=300, null=True, blank=True)
    archivo = models.FileField(null=True, blank=True)
    tipo = models.IntegerField(default=0, choices=TIPO_RECURSO)
    clase = models.ForeignKey('Clase', related_name='recursos', on_delete=models.PROTECT)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return "{}-{}".format(self.titulo, self.tipo)


class Profesor(models.Model):
    usuario = models.ForeignKey('Usuario', related_name='profesores', on_delete=models.PROTECT)
    descripcion = models.TextField(max_length=3000, blank=True, null=True)
    estudios_cursados = models.CharField(max_length=300, blank=True, null=True)

    def __str__(self):
        return "{}.{}".format(self.estudios_cursados, self.usuario.first_name)

    class Meta:
        app_label = 'backend'

class Contacto(models.Model):
    telefono = models.CharField(max_length=12)
    email = models.CharField(max_length=30, null=True, blank=True)
    web = models.CharField(max_length=30, null=True, blank=True)
    facebook = models.CharField(max_length=30, null=True, blank=True)
    twitter = models.CharField(max_length=30, null=True, blank=True)
    instagram = models.CharField(max_length=30, null=True, blank=True)
    youtube = models.CharField(max_length=30, null=True, blank=True)
    profesor = models.ForeignKey('Profesor', related_name='contacto', on_delete=models.PROTECT)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return "{}-{}".format(self.musico,self.telefono)


class Estudiante(models.Model):
    usuario = models.ForeignKey('Usuario', related_name='estudiante', on_delete=models.PROTECT)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return self.usuario.get_full_name()


class Categoria(models.Model):
    nombre = models.CharField(max_length=300)
    descripcion = models.TextField()
    slug = models.SlugField(max_length=200,null=True, editable=False, unique=True)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Categoria, self).save(*args, **kwargs)


class TiempoDuracion(models.Model):
    cantidad = models.IntegerField(default=0)
    tipo = models.IntegerField(default=0, choices=TIPO_DURACION)
    class Meta:
        app_label = 'backend'
    def __str__(self):
        return "{}{}".format(self.cantidad, self.get_tipo_display())
