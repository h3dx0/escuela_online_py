from django.contrib import admin
from escuela_online.backend.models import *
# Register your models here.


class RecursoAdmin(admin.TabularInline):
    model = Recursos


class ClaseAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'modulo']
    inlines = [
        RecursoAdmin,
    ]
class CursoAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'categoria','estado','fecha_creacion']


admin.site.register(Usuario)
admin.site.register(Estudiante)
admin.site.register(Profesor)
admin.site.register(Curso, CursoAdmin)
admin.site.register(Categoria)
admin.site.register(TiempoDuracion)
admin.site.register(Recursos)
admin.site.register(Modulo)
admin.site.register(Clase, ClaseAdmin)
