from django.apps import AppConfig


class FrontendConfig(AppConfig):
    name = 'escuela_online.frontend'
