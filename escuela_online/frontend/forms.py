from escuela_online.backend.models import Usuario
from django import forms
from django.forms import ModelForm

MES = (
    ('01','01'),
    ('02','02'),
    ('03','03'),
    ('04','04'),
    ('05','05'),
    ('06','06'),
    ('07','07'),
    ('08','08'),
    ('09','09'),
    ('10','10'),
    ('11','11'),
    ('12','12'),
)
ANNO = (
    ('2017','2017'),
    ('2018','2018'),
    ('2019','2019'),
    ('2020','2020'),
    ('2021','2021'),
    ('2022','2022'),
    ('2023','2023'),
    ('2025','2025'),
    ('2025','2025'),
)

class UsuarioForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'E-mail'}), required=True)
    username = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Confirmar E-mail'}), required=True)
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña'}), required=True)
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirmar contraseña'}), required=True)


class PerfilUsuario(ModelForm):
    class Meta:
        model = Usuario
        fields = ['first_name', 'last_name', 'avatar']


class PagoCursoTarjetaForm(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre'}), required=True)
    apellido = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Apellido'}), required=True)
    numero = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Número de tarjeta'}), required=True)
    mes = forms.ChoiceField(choices=MES, required=True)
    anno = forms.ChoiceField(choices=ANNO, required=True)
    cvv = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'CVV'}), required=True)
