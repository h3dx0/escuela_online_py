from escuela_online.backend.models import *
from django.conf import settings
from django.core.mail import send_mail


def obtener_menu_header(request):
    categorias = Categoria.objects.all()
    profesores = Profesor.objects.all()
    return {'categorias': categorias, 'profesores':profesores}


