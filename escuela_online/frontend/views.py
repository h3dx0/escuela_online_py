import re

from django.http.response import HttpResponse
from django.shortcuts import render
from escuela_online.backend.models import Curso, Categoria, Clase, Contacto,Estudiante,Modulo,Profesor,Recursos\
    ,TiempoDuracion,Usuario
from escuela_online.frontend.forms import *
from paypalrestsdk import Payment
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.db import IntegrityError
import logging
import json


# Create your views here.


def index(request):
    cursos_premium = Curso.objects.filter(premium=True).order_by('-fecha_creacion')[:5]
    context = {
        'cursos_premium': cursos_premium
    }
    return render(request, 'frontend/index.html', context)


def registro_usuario(request):
    form = UsuarioForm()
    msg = None
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            try:
                usuario = Usuario()
                usuario.username = form.cleaned_data['username']
                usuario.email = form.cleaned_data['email']
                usuario.set_password(form.cleaned_data['password2'])
                usuario.save()
                estudiante = Estudiante()
                estudiante.usuario = usuario
                estudiante.save()
                msg = {"msg": "Usuario creado con éxito.", "tipo": "ok"}
            except IntegrityError:
                msg = {
                    "msg": "Existe un problema al crear su usuario. Puede que ya exista. Revise sus datos e intente de nuevo",
                    "tipo": "error"}
    context = {
        'form': form,
        'msg': msg
    }
    return render(request, 'registration/registration_form.html', context=context)


# pagia de checkout antes de comprar
@login_required
def checkout(request, slug_curso):
    form_pago = PagoCursoTarjetaForm()
    try:
        estudiante = Estudiante.objects.get(usuario=request.user)
        curso_validacion = Curso.objects.filter(slug=slug_curso, estudiantes__in=[estudiante])
    except:
        curso_validacion = None

    if curso_validacion is not None and len(curso_validacion) > 0:
        context = {
            'msg': 'Ud ya compró este curso',
            'curso': None
        }
        return render(request, 'frontend/checkout.html', context)

    try:
        curso = Curso.objects.get(slug=slug_curso)
        context = {
            'curso': curso,
            'form': form_pago,
        }

    except Curso.DoesNotExist:
        context = {
            'msg': 'El curso que intenta comprar ya no está disponible',
            'curso': None
        }

    return render(request, 'frontend/checkout.html', context)


# editar perfil d usuario
@login_required
def perfil_usuario(request, id_usuario):
    usuario = request.user
    msg = None
    if request.method == 'POST':
        usuario_form = PerfilUsuario(request.POST)
        if usuario_form.is_valid():
            usuario.first_name = usuario_form.cleaned_data['first_name']
            usuario.last_name = usuario_form.cleaned_data['last_name']
            usuario.save()
            msg = "Perfil editado con éxito"

    else:
        usuario_form = PerfilUsuario(instance=usuario)
    try:
        estudiante = Estudiante.objects.get(usuario=request.user)
        cursos = estudiante.cursos.all()
    except Estudiante.DoesNotExist:
        cursos = None

    context = {
        'form': usuario_form,
        'usuario': usuario,
        'msg': msg,
        'cursos': cursos,
        'perfil': 1
    }
    return render(request, 'frontend/perfil_usuario.html', context=context)


# listado de cursos d una categoria
def listado_cursos(request, slug_categoria):
    categoria = Categoria.objects.get(slug=slug_categoria)
    cursos = Curso.objects.filter(categoria=categoria)
    context = {'cursos': cursos, 'categoria': categoria}
    return render(request, 'frontend/listado_cursos_categoria.html', context=context)


# listado de todas las categorias
def listado_categorias(request):
    categorias = Categoria.objects.all()
    context = {'categorias': categorias}
    return render(request, 'frontend/listado_categorias.html', context=context)


# listado de modulos de un curso
def descripcion_curso(request, slug_curso):
    categorias = Categoria.objects.all()
    try:
        curso = Curso.objects.get(slug=slug_curso)
        modulos = curso.modulos.all().order_by('id')
    except Curso.DoesNotExist:
        curso = None
        modulos = None

    tiene_curso = estudiante_tiene_curso(request.user, curso.pk)
    context = {
        'tiene_curso': tiene_curso,
        'curso': curso,
        'categorias': categorias,
        'modulos': modulos}
    return render(request, 'frontend/descripcion_curso.html', context=context)


# listado de clases de un modulo
def listado_clases(request, slug_modulo):
    modulo = Modulo.objects.get(slug=slug_modulo)
    clases = Clase.objects.filter(modulo=modulo).order_by('-id')

    curso = modulo.curso
    if request.user.is_authenticated:
        tiene_curso = estudiante_tiene_curso(request.user, curso.pk)
    else:
        tiene_curso = None

    context = {
        'clases': clases,
        'modulo': modulo,
        'tiene_curso': tiene_curso,
        'curso': curso
    }
    return render(request, 'frontend/listado_clases.html', context=context)


# Detalle de la clase
@login_required
def detalle_clase(request, slug_clase):
    clase = Clase.objects.get(slug=slug_clase)
    modulo = clase.modulo
    clases = modulo.clases
    recursos = Recursos.objects.filter(clase=clase)

    curso = modulo.curso
    tiene_curso = estudiante_tiene_curso(request.user, curso.pk)

    context = {
        'clases': clases,
        'clase': clase,
        'recursos': recursos,
        'tiene_curso': tiene_curso
    }
    return render(request, 'frontend/detalle_clase.html', context=context)


@login_required
def mis_cursos(request):
    try:
        estudiante = Estudiante.objects.get(usuario=request.user)
        cursos = estudiante.cursos.all()
    except Estudiante.DoesNotExist:
        cursos = None
    context = {'cursos': cursos, 'perfil': 1}
    return render(request, 'frontend/perfil_usuario/mis_cursos.html', context=context)


logging.basicConfig(level=logging.INFO)


@csrf_exempt
@login_required
def ejecutar_pago(request, slug_curso):
    payment_id = request.POST['paymentID']
    payer_id = request.POST['payerID']
    payment = Payment.find(payment_id)
    if payment.execute({"payer_id": payer_id}):  # return True or False
        # TODO crear modelo para registrar transacciones
        try:
            curso = Curso.objects.get(slug=slug_curso)
            estudiante = Estudiante.objects.get(usuario=request.user)
            curso.estudiantes.add(estudiante)
            curso.save()
            print("Payment[%s] execute successfully" % (payment.id))

            json_data = {'msg': 'Pago completado', 'rc': 0}
            return HttpResponse(json.dumps(json_data), content_type='application/json')

        except Curso.DoesNotExist:
            json_data = {'msg': 'El curso que intenta comprar ya no está disponible', 'rc': -11}
            return HttpResponse(json.dumps(json_data), content_type='application/json')

    else:
        print(payment.error)
        json_data = {'msg': 'Pago rechazado', 'rc': -10}
        return HttpResponse(json.dumps(json_data), content_type='application/json')


# crear el pago del curso con paypal
@csrf_exempt
@login_required
def crear_pago(request, slug_curso):
    curso = Curso.objects.get(slug=slug_curso)
    descripcion = "Compra del curso {}en la plataforma Escuela Cubana".format(curso.titulo)
    payment = Payment({
        "payer": {
            "payment_method": "paypal"},
        "redirect_urls": {
            "return_url": settings.RETURN_URL,
            "cancel_url": settings.CANCEL_URL},
        "intent": "sale",
        "transactions": [{

            # ItemList
            "item_list": {
                "items": [{
                    "name": curso.titulo,
                    "sku": curso.id,
                    "price": curso.precio,
                    "currency": 'MXN',
                    "quantity": 1}]},

            # Amount
            # Let's you specify a payment amount.
            "amount": {
                "total": curso.precio,
                "currency": "MXN"},
            "description": descripcion}]})
    if payment.create():
        print("Payment[%s] created successfully" % (payment.id))
        json_data = {'id': payment.id}
        return HttpResponse(json.dumps(json_data), content_type='application/json')
    else:
        print("Error while creating payment:")
        print(payment.error)
        return HttpResponse(json.dumps(payment.error), content_type='application/json')


@csrf_exempt
@login_required
def crear_pago_tarjeta(request):
    curso_slug = request.POST['curso']
    curso = Curso.objects.get(slug=curso_slug)
    descripcion = "Compra del curso {}en la plataforma Escuela Cubana".format(curso.titulo)
    numero = request.POST['numero']
    mes = request.POST['mes']
    anno = request.POST['anno']
    cvv = request.POST['cvv']
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    payment = Payment({
        "payer": {
            "payment_method": "credit_card",
            # FundingInstrument
            # A resource representing a Payeer's funding instrument.
            # Use a Payer ID (A unique identifier of the payer generated
            # and provided by the facilitator. This is required when
            # creating or using a tokenized funding instrument)
            # and the `CreditCardDetails`
            "funding_instruments": [{
                # CreditCard
                # A resource representing a credit card that can be
                # used to fund a payment.
                "credit_card": {
                    "type": get_tarjeta_tipo(numero),
                    "number": numero,
                    "expire_month": mes,
                    "expire_year": anno,
                    "cvv2": cvv,
                    "first_name": nombre,
                    "last_name": apellido,
                }}]},
        "intent": "sale",
        "transactions": [{
            # ItemList
            "item_list": {
                "items": [{
                    "name": curso.titulo,
                    "sku": curso.id,
                    "price": curso.precio,
                    "currency": 'MXN',
                    "quantity": 1}]},

            # Amount
            # Let's you specify a payment amount.
            "amount": {
                "total": curso.precio,
                "currency": 'MXN'},
            "description": descripcion}]})
    if payment.create():
        print("Payment[%s] created successfully" % (payment.id))
        if payment.execute({"payer_id": payment.payer.payer_info.payer_id}):  # return True or False
            # TODO crear modelo para registrar transacciones
            try:
                curso = Curso.objects.get(slug=curso_slug)
                estudiante = Estudiante.objects.get(usuario=request.user)
                curso.estudiantes.add(estudiante)
                curso.save()
                print("Payment[%s] execute successfully" % (payment.id))
                context = {'msg': 'Pago comprado correctamene', 'rc': 0}
                return render(request, 'frontend/pago_curso_respuesta.html', context=context)

            except Curso.DoesNotExist:
                context = {'msg': 'El curso que intenta comprar ya no está disponible', 'rc': -11}
                return render(request, 'frontend/pago_curso_respuesta.html', context=context)

        else:
            print(payment.error)
            context = {'msg': 'Error al procesar su pago.', 'rc': -11}
            return render(request, 'frontend/pago_curso_respuesta.html', context=context)
    else:
        print("Error while creating payment:")
        print(payment.error)
        context = {'msg': 'Ha ocurrido un error con sus datos.Verifíquelos e intente de nuevo. Gracias', 'rc': -11}
        return render(request, 'frontend/pago_curso_respuesta.html', context=context)


def get_tarjeta_tipo(numero):
    AMEX_CC_RE = re.compile(r"^3[47][0-9]{13}$")
    VISA_CC_RE = re.compile(r"^4[0-9]{12}(?:[0-9]{3})?$")
    MASTERCARD_CC_RE = re.compile(r"^5[1-5][0-9]{14}$")
    DISCOVER_CC_RE = re.compile(r"^6(?:011|5[0-9]{2})[0-9]{12}$")

    CC_MAP = {"amex": AMEX_CC_RE, "visa": VISA_CC_RE,
              "mastercard": MASTERCARD_CC_RE, "discover": DISCOVER_CC_RE}

    for type, regexp in CC_MAP.items():
        if regexp.match(str(numero)):
            return type
    return None


# Comprar curso x usuario, aqui se asigna el curso al usuario
def comprar_curso(request):
    usuario = request.GET['usuario']
    curso = request.GET['curso']
    try:
        curso_obj = Curso.objects.get(slug=curso, estado=0)
    except Curso.DoesNotExist:
        json_data = {
            'rc': -30,
            'msg': 'El curso no se encuentra disponible.Contáctenos',
        }
        return HttpResponse(json.dumps(json_data), content_type='application/json')

    try:
        usuario_obj = Usuario.objects.get(pk=usuario)
        estudiante = Estudiante.objects.get(usuario=usuario_obj)
    except Usuario.DoesNotExist:
        json_data = {
            'rc': -10,
            'msg': 'El usuario no se encuentra disponible. Contáctenos',
        }
        return HttpResponse(json.dumps(json_data), content_type='appliaction/json')

    curso_obj.estudiantes.add(estudiante)
    curso_obj.save()

    json_data = {
        'rc': 0,
        'msg': 'Ok',
    }
    return HttpResponse(json.dumps(json_data), content_type='application/json')


def estudiante_tiene_curso(usuario, curso_id):
    try:
        estudiante = Estudiante.objects.get(usuario=usuario)
        estudiante.cursos.filter(pk=curso_id)
        return True
    except Exception:
        return False
