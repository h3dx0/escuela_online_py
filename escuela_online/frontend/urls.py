from django.urls import path
from escuela_online.frontend import views

app_name = 'frontend'

urlpatterns = [
    path('', views.index, name='index'),
    path('curso/<slug:slug_curso>', views.descripcion_curso, name='descripcion_curso'),
    path('checkout/<slug:slug_curso>', views.checkout, name='checkout'),
    path('perfil/usuario/<int:id_usuario>', views.perfil_usuario, name='perfil_usuario'),
    path('registro', views.registro_usuario, name='registro_usuario'),
    path('usuario/mis-cursos', views.mis_cursos, name='mis_cursos'),
    path('modulo/<slug:slug_modulo>', views.listado_clases, name='listado_clases'),
    path('cursos/<slug:slug_categoria>', views.listado_cursos, name='listado_cursos'),
    path('categorias', views.listado_categorias, name='listado_categorias'),
    path('detalle/clase/<slug:slug_clase>', views.detalle_clase, name='detalle_clase'),
    # path('comprar/curso/resumen/$', views.comprar_curso, name='comprar_curso'),
    path('crear/pago/curso/<slug:slug_curso>', views.crear_pago, name='crear_pago'),
    path('ejecutar/pago/curso/<slug:slug_curso>', views.ejecutar_pago, name='ejecutar_pago'),
    # path('respuesta/pago/curso/$', views.respuesta_pago_curso, name='respuesta_pago_curso'),
    # path('pago/curso/tarjeta$', views.crear_pago_tarjeta, name='crear_pago_tarjeta'),

]
